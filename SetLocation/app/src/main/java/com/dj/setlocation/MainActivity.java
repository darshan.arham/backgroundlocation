package com.dj.setlocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.dj.setlocation.databinding.ActivityMainBinding;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /**
     * DATABASE url:: https://locationdemo-75396-default-rtdb.firebaseio.com/:
     */

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String ROUTE = "Route1";
    private ActivityMainBinding binding;
    LocationManager locationManager;
    private Activity activity;
    boolean gps_enabled = false, network_enabled = false, isResume = false;
    Double latitude = null, longitude = null;

    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        activity = MainActivity.this;

        checkPermission();

        //         Write a message to the database
        myRef = FirebaseDatabase.getInstance().getReference(ROUTE);
//
//        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                LocationModel model = dataSnapshot.getValue(LocationModel.class);
                //                Log.d(TAG, "Value is: " + value);
                if (model != null) {
                    Log.d(TAG, "From Database Latitude" + model.getLatitude() + "\n Longitude" + model.getLongitude());
                    binding.txtDB.setText("From Database " + "Route" + model.getRouteName() + "\nLatitude" + model.getLatitude() + "\n Longitude" + model.getLongitude());
                } else {
                    Log.d(TAG, "From Database received empty model");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        });

/*
        //FOR PIPMODE
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.S) {
            binding.pipModeBtn.setVisibility(View.GONE);
            setPictureInPictureParams(new PictureInPictureParams.Builder().setAutoEnterEnabled(true).build());
        } else {
            binding.pipModeBtn.setOnClickListener(v -> enterPictureInPictureMode());
        }
*/


    }

    private void checkPermission() {
        PermissionListener permissionlistener = new PermissionListener() {

            @Override
            public void onPermissionGranted() {

                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                try {
                    gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try {
                    network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                if (!gps_enabled && !network_enabled) {
                    // notify user
                    new AlertDialog.Builder(activity).setMessage("To continue, turn on device location.").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (!gps_enabled || !network_enabled) {
//                                        binding.currentLocationBtn.setVisibility(View.GONE);
                                Log.d(TAG, "isGpsEnabled : " + gps_enabled + "\nisNetworkEnabled : " + network_enabled);
                            }
                        }
                    }).show();
                } else {
                    Log.d(TAG, "isGpsEnabled : " + gps_enabled + "\nisNetworkEnabled : " + network_enabled);
                    startService(new Intent(activity, Service3.class));

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, listener);
                    /*if (mPref.getString("service", "").matches("")) {
                        medit.putString("service", "service").commit();

                        Intent intent = new Intent(getApplicationContext(), GoogleService.class);
                        startService(intent);

                    } else {
                        Toast.makeText(getApplicationContext(), "Service is already running", Toast.LENGTH_SHORT).show();
                    }*/
                    //                    startForegroundService(new Intent(activity, LocationService.class));
//                    startService(new Intent(activity, LocationService.class));

                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(activity, "permission_denied" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
//                checkPermission();
            }
        };


        TedPermission.with(activity).setPermissionListener(permissionlistener)
                .setPermissions(Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION).setDeniedMessage("If you reject permission,you won't be able to access this page.\nPlease allow permissions.").check();

    }

    public final LocationListener listener = new LocationListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onLocationChanged(@NonNull Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

//            LocationModel model = new LocationModel();
//            model.setLatitude(latitude);
//            model.setLongitude(longitude);
//            model.setRouteName(ROUTE);
//            myRef.setValue(model);

            Log.d(TAG, "Current Latitude" + latitude + "\nCurrent Longitude" + longitude);

            binding.txtLocation.setText("Current Route" + ROUTE + "\nCurrent Latitude" + latitude + "\nCurrent Longitude" + longitude);

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {

        }
    };

    @Override
    protected void onResume() {
        if (isResume) {
            checkPermission();
        } else {
            isResume = true;
        }
        super.onResume();
    }

/*
//    for PIPMODE
    @Override
    public void onUserLeaveHint() {
//        if (iWantToBeInPipModeNow()) {
        enterPictureInPictureMode();
//        }
    }
*/


}