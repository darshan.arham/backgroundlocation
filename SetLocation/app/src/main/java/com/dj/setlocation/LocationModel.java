package com.dj.setlocation;

public class LocationModel {

    public String routeName;
    public double latitude;
    public double longitude;

    public LocationModel() {
    }

    public LocationModel(String routeName, double latitude, double longitude) {
        this.routeName = routeName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
